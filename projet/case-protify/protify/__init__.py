import traceback
try:
  from .app import app
  from .app import db
  import protify.views
  import protify.commands
  import protify.models

except:
  traceback.print_last()
