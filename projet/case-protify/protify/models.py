import yaml, os.path

from .app import db
from flask_login import UserMixin
from .app import login_manager
from sqlalchemy import *
from sqlalchemy.dialects.postgresql import ARRAY

class Author(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))

    def __repr__(self):
        return "<Author (%d) %s>" % (self.id, self.name)

class Music(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100))
    img = db.Column(db.String(1000))
    
    
    parent = db.Column(db.String(100))
    genre = db.Column(db.String(100))
    author_id = db.Column(db.Integer, db.ForeignKey("author.id"))
    author = db.relationship("Author", backref=db.backref("music", lazy="dynamic"))

    def __repr__(self):
        return "<Music (%d) %s>" % (self.id, self.title)

Musics = yaml.safe_load(open(os.path.join(os.path.dirname(__file__),"extrait.yml")))

def get_sample():
    return Musics[0:10]

def get_sample_fromBD():
    return Music.query.limit(10).all()

def get_author(id):
    return Author.query.filter(Author.id==id).one()

class User(db.Model, UserMixin):
    username = db.Column(db.String(50), primary_key=True)
    password = db.Column(db.String(64))

    def get_id(self):
        return self.username


@login_manager.user_loader
def load_user(username):
    return User.query.get(username)