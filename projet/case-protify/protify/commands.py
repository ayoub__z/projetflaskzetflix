import click
from .app import app, db
@app.cli.command()
@click.argument('filename')

def loaddb(filename):

    '''Creates the tables and populates them with data.'''

    # création de toutes les tables
    db.create_all()

    # chargement de notre jeu de données
    import yaml
    musics = yaml.safe_load(open(filename))

    # import des modèles
    from .models import Author, Music

    # première passe: création de tous les auteurs
    authors = {}
    for m in musics:
        a = m["by"]
        if a not in authors:
            o = Author(name=a)
            db.session.add(o)
            authors[a] = o
    db.session.commit()

    # deuxième passe: création de tous les livres
    for m in musics:
        a = authors[m["by"]]
        o = Music(title = m["title"], id = m["entryId"], img = m["img"]  ,genre = m["genre"] , parent = m["parent"], author_id=a.id)
        db.session.add(o)
    db.session.commit()

@app.cli.command()
def syncdb():
    '''Creates all missing tables.'''
    db.create_all()



@app.cli.command()
@click.argument('username')
@click.argument('password')

def newuser(username , password):
    '''Adds a new user.'''
    from .models import User
    from hashlib import sha256
    m = sha256()
    m.update(password.encode())
    u = User(username=username, password=m.hexdigest())
    db.session.add(u)
    db.session.commit()